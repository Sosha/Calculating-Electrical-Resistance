#!/usr/bin/python3


# Librarys
from colored import fg, bg, attr
import re


# Colors
BlackBGC = bg('#000000')
BrownBGC = bg(94)
RedBGC = bg('#ff0000')
OrangeBGC = bg('202')
YellowBGC = bg('11')
GreenBGC = bg('28')
BlueBGC = bg('21')
PurpleBGC = bg('129')
GrayBGC = bg('#808080')
WhiteBGC = bg('#FFFFFF')
SilverBGC = bg('250')
GoldBGC = bg('220')
BlackFGC = fg(0)
ResC = attr('reset')


# Function
def compile(inp):
   colores = {
       "bc|black|0" : "0",
       "br|brown|1" : "1",
       "gr|green|5" : "5",
       "bl|blue|6" : "6",
       "gy|gray|8" : "8",
       "r|red|2" : "2",
       "o|orange|3" : "3",
       "y|yellow|4" : "4",
       "p|purple|7" : "7",
       "w|white|9" : "9",
       "s|silver|5" : "5",
       "g|gold|10" : "10"
    }
   for i in    colores.keys():
       inp = re.sub(i,colores[i],inp)
   return inp

def calc(num1, num2, num3):
   number = int(str(num1)+str(num2))
   zeroes = 10**int(num3)
   return int(number*zeroes)


# Print Hint
print(f"{BlackBGC} Black  = 0 = bc  {ResC}\n{BrownBGC} Brown  = 1 = br  {ResC}\n{RedBGC} Red    = 2 = r   {ResC}\n{OrangeBGC} Orange = 3 = o   {ResC}\n{YellowBGC}{BlackFGC} Yellow = 4 = y   {ResC}\n{GreenBGC} Green  = 5 = gr  {ResC}\n{BlueBGC} Blue   = 6 = bl  {ResC}\n{PurpleBGC} Purple = 7 = p   {ResC}\n{GrayBGC} Gray   = 8 = gy  {ResC}\n{WhiteBGC}{BlackFGC} White  = 9 = w   {ResC}\n{SilverBGC}{BlackFGC} Silver = s = %5  {ResC}\n{GoldBGC}{BlackFGC} Gold   = g = %10 {ResC}\n\n")


# Calculation
num1 = compile(input("First Color or Number: "))
num2 = compile(input("Second Color on Number: "))
num3 = compile(input("Third Color or Number: "))
num4 = compile(input("Fourth Color or Number: "))


# Variable
res = calc(num1,num2,num3)
tel = res*int(compile(num4))/100


# Result
print ("\n\nResults:\nTotal: (WithOut Tolerance) [=]", int(res),"±" + " %" + num4)
print ("Sum: (With Tolerance) [+]", int(res+tel))
print ("Subtraction: (With Tolerance) [-]", int(res-tel))
